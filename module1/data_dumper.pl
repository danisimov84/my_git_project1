use strict;
use warnings;    
use Data::Dumper;

# dump of object's symbol table:
foreach my $className (@ARGV)
{
    print "symbols in $className:";

    eval "require $className";
    die "Can't load $className: $@" if $@;

    no strict 'refs';
    print Dumper(\%{"main::${className}::"});
}